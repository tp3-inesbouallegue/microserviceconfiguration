package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
@EnableConfigServer
public class Tp3ComfigurationApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tp3ComfigurationApplication.class, args);
	}

	@Value("${nom.password}")
	private String password;
	@GetMapping("/test")
	public String getProperties() {
		return password;
	}
}
